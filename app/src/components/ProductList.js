import { useState, useEffect } from "react";
import ProductItem from '../components/ProductItem';
import Loader from "./Loader";

const ProductList = ({ setSelectedProducts }) => {

  const [products, setProducts] = useState({
    isLoaded: false,
    items: []
  });

  useEffect(() => {
    fetch('http://localhost:5000/products')
      .then((response) => response.json())
      .then(
        (data) => {
          setProducts({
            isLoaded: true,
            items: data
          });
        }
      );
  }, []);

  const addItem = ({ target }) => {
    const item = target.value;
    setSelectedProducts((prevItens) => [item, ...prevItens]);
  };

  const { isLoaded, items } = products;

  if (!isLoaded) {
    return <Loader />;
  } else {
    return (
      <div className="product-list">
        {items.map((product) => (
          <ProductItem
            key={product._id}
            name={product.name}
            description={product.description}
            price={product.price}
            addItem={addItem} />
        ))}
      </div>
    )
  }
}

export default ProductList