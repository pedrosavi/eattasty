import { useState, useEffect } from "react";

const Cart = ({ selectedProducts, setSelectedProducts }) => {

  const [quantity, setQuantity] = useState(0);

  const removeItem = ({ target }) => {
    setSelectedProducts((prevItens) => {
      const items = prevItens.filter((item, index) => {
        return Number(index) !== Number(target.value);
      })

      return items;
    })

    setQuantity(selectedProducts.length - 1);
  };

  const getWhatsAppMessage = () => {
    const restaurantWhatsAppNumber = '+5548991221611';
    const header = '*Restaurante PT - Meu Pedido*%0a';

    let body = '';
    selectedProducts.map((item, index) => (
      body += `*${encodeURI(item)}%0a`
    ))

    return `https://api.whatsapp.com/send?phone=${restaurantWhatsAppNumber}&text=${header}%0a${body}`;
  };

  const sendToWhatsApp = () => {
    window.location.href = getWhatsAppMessage();
  };

  useEffect(() => {
    setQuantity(selectedProducts.length);
  }, [selectedProducts]);

  return (
    <div className="my-5 text-center">
      {quantity ? (
        <p>Carrinho</p>
      ) : (
        <div className="bg-light p-5 rounded mt-3  col-md-6 mb-4 col-lg-4 mx-auto">
          Nenhum produto no carrinho
        </div>
      )}
      <ul className="list-group col-md-6 mb-4 col-lg-4 mx-auto">
        {selectedProducts.map((item, index) => (
          <li key={index} className="list-group-item d-flex justify-content-between align-items-center">
            <div className="ms-2 me-auto">
              {item}
            </div>
            <button className="btn btn-sm btn-outline-danger" value={index} onClick={removeItem}>x</button>
          </li>
        ))}
      </ul>

      <p>
        Você será redirecionado para o WhatsApp <br />
        após finalizar o pedido!
      </p>
      <button className="btn btn-lg btn-warning" onClick={sendToWhatsApp}>
        <span className="badge bg-dark">{quantity}</span> Enviar Pedido
      </button>
    </div>
  )
}

export default Cart
