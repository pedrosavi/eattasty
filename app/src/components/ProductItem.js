const ProductItem = ({ name, description, price, addItem }) => {
  return (
    <div className="product-list__item mb-3">
      <div className="card card-product">
        <div className="card-body d-flex justify-content-between">
          <div className="card-product__info">
            <h5 className="card-title">{name}</h5>
            <h6 className="card-subtitle mb-2 text-muted">€ {price}</h6>
            <p className="card-text">{description}</p>
          </div>
          <div className="card-product__actions mt-auto">
            <button className="btn btn-warning" value={name} onClick={addItem}>+ Adicionar</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProductItem
