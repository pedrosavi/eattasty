const Header = () => {
  return (
    <header className="p-5 text-center">
      <h1 className="display-5 fw-bold">Restaurante PT</h1>
      <p className="lead mb-0">Selecione seu pedido e envie via whatsapp!</p>
    </header>
  )
}

export default Header
