const Footer = () => {
  return (
    <footer className="mt-5 text-center">
      <p>EatTasty Delivery via WhatsApp, by <a href="https://www.instagram.com/eattasty.pt/" className="text-warning">@eattasty.pt</a>.</p>
    </footer>
  )
}

export default Footer
