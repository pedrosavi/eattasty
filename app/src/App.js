import { useState } from "react";
import Header from './components/Header';
import Footer from './components/Footer';
import ProductList from './components/ProductList';
import Cart from './components/Cart';

function App() {

  const [selectedProducts, setSelectedProducts] = useState([]);

  return (
    <div className="container">
      <Header />
      <main className="content">
        <ProductList setSelectedProducts={setSelectedProducts} />
        <Cart selectedProducts={selectedProducts} setSelectedProducts={setSelectedProducts} />
      </main>
      <Footer />
    </div>
  );
}

export default App;
