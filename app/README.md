# EatTasty - App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Follow this steps to start the app

PS: Don`t forget to start the server api before

### 1 - Install dependencies

In the project directory, you can run:

### `yarn install`

### 2 - Start the app

Run:

### `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.