import express from 'express';
import { } from 'dotenv/config';
import cors from 'cors';
import authController from './controllers/authController';
import productController from './controllers/productController';

const app = express();

app.use(cors());
app.use(express.json());

app.use('/auth', authController);
app.use('/products', productController);

app.listen(process.env.PORT, () => {
    console.log(`Connected on port ${process.env.PORT}`);
});