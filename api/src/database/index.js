import Mongoose from 'mongoose';

Mongoose.connect(process.env.DB_CONNECTION, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
}, (error) => {
  if (!error) return;
  console.log('Error connection!', error);
})

Mongoose.Promise = global.Promise;

export default Mongoose;