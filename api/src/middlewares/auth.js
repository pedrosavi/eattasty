import jwt from 'jsonwebtoken';

export default (req, res, next) => {
  const authHeader = req.header('authorization');

  if (!authHeader)
    return res.status(400).send('No token provided!');

  const parts = authHeader.split(' ');

  if (!parts.length === 2)
    return res.status(400).send('Token malformatted!');

  const [schema, token] = parts;

  if (!/^Bearer$/i.test(schema))
    return res.status(400).send('Token malformatted!');

  jwt.verify(token, process.env.TOKEN_SECRET, (err, decoded) => {
    if (err)
      return res.status(400).send(err);

    req.userId = decoded._id;
    return next();
  });
};