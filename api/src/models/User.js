import Database from '../database';
import bcrypt from 'bcrypt';

const UserSchema = Database.Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      unique: true,
      required: true,
      lowecase: true
    },
    password: {
      type: String,
      required: true,
      select: false
    },
  },
  {
    timestamps: true
  }
);

UserSchema.pre('save', async function (next) {
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(this.password, salt);

  this.password = hash;

  next();
});

const User = Database.model('User', UserSchema);

export default User;