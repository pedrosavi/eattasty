import Database from '../database';

const ProductSchema = Database.Schema(
  {
    name: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    price: {
      type: Number,
      required: true
    }
  },
  {
    timestamps: true
  }
);

const Product = Database.model('Product', ProductSchema);

export default Product;