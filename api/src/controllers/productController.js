import express from 'express';
import auth from '../middlewares/auth';
import Product from '../models/Product';

const router = express.Router();

// List Products
router.get('/', async (req, res) => {
  try {
    const products = await Product.find();
    res.send(products);
  } catch (err) {
    res.status(400).send({ message: err });
  }
});

// Create Product
router.post('/', auth, async (req, res) => {
  try {
    const product = await Product.create({ ...req.body });
    return res.send(product);
  } catch (err) {
    return res.status(400).send({ message: err });
  }
});

// Show Product
router.get('/:id', async (req, res) => {
  try {
    const product = await Product.findById(req.params.id);
    res.send(product);
  } catch (err) {
    res.status(400).send({ message: err });
  }
});

// Delete Product
router.delete('/:id', auth, async (req, res) => {
  try {
    const product = await Product.remove({ _id: req.params.id });
    res.send(product);
  } catch (err) {
    res.status(400).send({ message: err });
  }
});

// Update Product
router.patch('/:id', auth, async (req, res) => {
  try {
    const product = await Product.updateOne(
      { _id: req.params.id },
      { $set: { ...req.body } }
    );

    res.send(product);
  } catch (err) {
    res.status(400).send({ message: err });
  }
});

export default router;