import express from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import User from '../models/User';

const router = express.Router();

const generateToken = params => {
  return jwt.sign(params, process.env.TOKEN_SECRET)
}

// Register User
router.post('/register', async (req, res) => {
  try {
    const user = await User.create(req.body);

    user.password = undefined;

    return res.send({
      user
    });
  } catch (err) {
    return res.status(400).send({ message: err });
  }
});

// Authenticate User
router.post('/authenticate', async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email }).select('+password');

  if (!user)
    return res.status(400).send({ message: 'E-mail not found!' });

  const validPass = await bcrypt.compare(password, user.password);

  if (!validPass)
    return res.status(400).send({ message: 'Invalid password!' });

  user.password = undefined;

  return res.send({
    token: generateToken({ _id: user._id })
  });
});

export default router;