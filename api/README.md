# EatTasty - Api

Simple REST API with JWT authentication using NodeJS and MongoDB

## Follow this steps to start the server

### 1 - Install dependencies

In the project directory, you can run:

### `npm install`

### 2 - Start the server

Run:

### `npm start`

## Making requests

### 1 - Authentication

To make request to the API you need a token.
You can just use my token: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MGEyYzMwMTE2MWYyYzE1YmUzNzkwOTIiLCJpYXQiOjE2MjEyNzk1NjN9.A-U6_nd1VKkfeeDfDxYohCGpIBtIQCrgBswvbhg4xgU`

Or create a user yourself making a `POST` request to `/auth/register` sending a json in the body with your new user data.

```
{
    "name": "Helder",
    "email": "helder.rossa@eattasty.com",
    "password": "eat1234"
}
```

Then, make a `POST` request to `/auth/authenticate` to get your own token sending your credentials in the body:

```
{
    "email": "helder.rossa@eattasty.com",
    "password": "eat1234"
}
```

After you get your token, make the requests sending the token in the header like this: `Authorization: Bearer <token>` to the product collection

PS: You just need the token to create, update and delete products.